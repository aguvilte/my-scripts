# !/bin/sh

foldername=$1
param_valido=0
no_param=1

if [ $# -eq 0 ]; then
    param_valido=0
    no_param=0
    echo 'ERROR!' 
    echo '\tDebe ingresar despues del comando el nombre del proyecto que quiere crear.'
    echo '\t(ejemplo: sh ~/scripts/vleasoft-php.sh mi_proyecto)'
fi

if [ $# -eq 2 ]; then
    if [ $2 = '-crearbd' ]; then
        param_valido=1

        echo 'Creando proyecto '$foldername'\n'

        hostdb_default="localhost"
        read -p "Host de la base de datos [por defecto: $hostdb_default]: " hostdb
        : ${hostdb:=$hostdb_default}
        
        namedb_default=$foldername
        read -p "Nombre de la base de datos [por defecto: $namedb_default]: " namedb
        : ${namedb:=$namedb_default}

        userdb_default="root"
        read -p "Usuario de la base de datos [por defecto: $userdb_default]: " userdb
        : ${userdb:=$userdb_default}
        
        passdb_default="root"
        read -p "Contraseña de la base de datos [por defecto: $passdb_default]: " passdb
        : ${passdb:=$passdb_default}
    else
        no_param=0
        echo 'El parametro '$2' no es valido. El unico parametro valido es -crearbd.\nIngrese nuevamente el comando'
    fi
fi

if [ $no_param -eq 1 ]; then
    mkdir $foldername
    mkdir $foldername/'assets'
    mkdir $foldername/'assets/js'
    mkdir $foldername/'assets/css'
    mkdir $foldername/'assets/img'
    mkdir $foldername/'src'
    mkdir $foldername/'src/config'

    touch $foldername/'index.php'
    echo '<?php' > $foldername/'index.php'
    touch $foldername/'src/config/config.php'
    echo '<?php' > $foldername/'src/config/config.php'
fi

if [ $param_valido -eq 1 ]; then
    config_file=$foldername/'src/config/config.php'

    echo '/*\n\tEste archivo es el que tiene las credenciales para\n\tpoder hacer uso de la base de datos\n*/\n' >> $config_file
    echo '$dbhost = '$hostdb';' >> $config_file
    echo '$dbname = '$namedb';' >> $config_file
    echo '$dbuser = '$userdb';' >> $config_file
    echo '$dbpass = '$passdb';\n' >> $config_file
fi